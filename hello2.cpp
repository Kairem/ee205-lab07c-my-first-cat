///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 01_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {
   std::cout << "Hello World." << std::endl;
}
